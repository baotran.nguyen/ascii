#Write a function to return the uppercase version of a letter
#Write a function to return the lowercase version of a letter
#Write a function that returns true if the letter is an alphabet
#Write a function that returns true if an element is a digit
#Write a function to determine if the given character is a special character or not
def upper(char):
    return chr(ord(char) - 32)
    
def lower(char):
    return chr(ord(char) + 32)
    
def isAlpha(char):
    if(90 >= ord(char) >= 65 or 122 >= ord(char) >= 97):
        return True
    return False
    
def isDigit(char):
    if(57 >= ord(char) >= 48):
        return True
    return False

def isSpecial(char):
    if(not(90 >= ord(char) >= 65 or 122 >= ord(char) >= 97) and not (57 >= ord(char) >= 48)):
        return True
    return False
    
    
print("A and " + upper('a'))
print("B and " + lower('B'))

print("True: " + str(isAlpha('a')))
print("False: " + str(isAlpha('(')))

print("True: " + str(isDigit('1')))
print("False: " + str(isDigit('b')))

print("False: " + str(isSpecial('1')))
print("False: " + str(isSpecial('b')))
print("True: " + str(isSpecial('@')))
